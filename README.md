# AI FB CHAT BOT

AI BOT for facebook messenger platform, written in Node.js

## Getting Started

```
npm run start
yarn run start
```

### LIST OF AVAILABLE BOT COMMANDS

* joke - send random Chack Norris joke
* status - get the current status of the user (id & jokesbalance)
* reset - skip waiting for 24h and reset the jokes balance back to 10
* help - list all available bot commands

