'use strict'
const fs = require('fs');
const express = require('express');
const request = require('request');

const app = express();

// import controllers
const RootController = require('./controllers/root.controller');
const BotController = require('./controllers/bot.controller');
// const UserController = require('./controllers/user.controller');

// index
app.get('/', RootController.root);

// for facebook verification
app.get('/webhook/', BotController.verifyToken);

// to post data
app.post('/webhook/', BotController.botInteractions);

// privacy policy and terms of service static files for facebook verification
app.get('/termsofservice', RootController.termsofservice);
app.get('/privacypolicy', RootController.privacypolicy);

module.exports = app;