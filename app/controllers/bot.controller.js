const request = require('request');
const apiBaseUrl = process.env.API_URL || 'http://aichatbot.ga';
import Promise from 'promise';
import superagent from 'superagent-bluebird-promise';
import User from '../models/user';

exports.verifyToken = function (req, res) {
	if (req.query['hub.verify_token'] === process.env.VERIFY_TOKEN) {
		res.send(req.query['hub.challenge'])
	} else {
		res.send('Error, wrong token')
	}
};

exports.botInteractions = function (req, res) {
	let messaging_events = req.body.entry[0].messaging;

	if (messaging_events) {
		for (let i = 0; i < messaging_events.length; i++) {
			let event = req.body.entry[0].messaging[i];
			let senderId = event.sender.id;
			if (event.message && event.message.text) {
				let text = event.message.text;


				if (text === 'joke') {
					/*  0  CHECK if fbUserID already exists in DB */
					let usrObj;
					let registeredUser;

					User.findOne({ userId: senderId }).exec().then((foundUserObj) => {
					usrObj = foundUserObj;
						// 2. read existing  FBuserId from db with  X jokes
						if (usrObj) {

							// 2.1   if  jokesBalance > 0   ==> sendJoke() and then update userIdObj with ( X - 1 ) jokes
							if (usrObj.jokesBalance > 0) {
								// send the joke

									// decrement jokesBalance
								User.update({ userId: senderId }, { $inc: { jokesBalance: -1 }, lastJokeSentAt: Math.floor(Date.now() / 1000) }).exec().then((foundUserObj) => {
									// get the joke
									superagent.get('http://api.icndb.com/jokes/random').then((res) => {
										sendTextMessage(senderId, res.body.value.joke); // send the joke to user

									})
								}).catch((error) => {
										console.log(error);
									});


							}

							// 2.2   else  (jokesBalance=0)
							if (usrObj.jokesBalance === 0) {
								const currentTimeStamp = Math.floor(Date.now() / 1000);
										// 2.2.1  if  ( currentTmStmp - lastJokeSentAt ) > 24H  ==>  reset()  (update jokesBalance to 10  or REMOVE user from registry)
									if (currentTimeStamp - usrObj.lastJokeSentAt > 86400) {

										User.update({ userId: senderId }, {jokesBalance: 9, lastJokeSentAt: Math.floor(Date.now() / 1000) }).exec().then((foundUserObj) => {
											superagent.get('http://api.icndb.com/jokes/random').then((res) => {
												sendTextMessage(senderId, res.body.value.joke); // send the joke to user
											});
										});

									} else {
										// 2.2.2 else  ( currentTmStmp - lastJokeSentAt ) < 24H ===>  send warning message "wait for 24 hours until next 10 jokes"
										sendTextMessage(senderId, "please wait 24 hours to have another 10 jokes on your JokesBalance");
									}
							}

						}
						// 1. register new FBuserId with ( 10 - 1 ) jokes in DB
						else {

							User.update({}, { userId: senderId, jokesBalance: 9 }, {upsert: true}).exec().then((foundUserObj) => {

								superagent.get('http://api.icndb.com/jokes/random').then((res) => {
									sendTextMessage(senderId, res.body.value.joke);

								}).catch((error) => {
									console.log(error);
								});
							});
						}
					}).catch((err) => {   console.log("error " + err);  });


				} else if (text === 'help') {
					sendTextMessage(senderId, `• type 'joke' to get a random joke 
					\n• type 'reset' to reset jokesBalance back to 10 
					\n ( Warning! The bot gives only 10 jokes per 24 hours, so you could either wait 24h to get another 10 jokes on balance or type 'reset' to renew the balance immediately )
					\n• type 'help' to see the list of supported commands that can be given to this AI chat bot `);

				} else if (text === 'status') {
					User.findOne({ userId: senderId }).exec().then((foundUserObj) => {
						sendTextMessage(senderId,
							`• you are recognized with id: ${foundUserObj.userId}
							\n• your jokes balance is: ${foundUserObj.jokesBalance}`);
					}).catch((err) => {   console.log("error " + err);  });
					

				} else if (text === 'reset') {
					// reset the jokesBalance back to default 10
					User.update({ userId: senderId }, { jokesBalance: 10 }).exec().then((foundUserObj) => {
						sendTextMessage(senderId, "RESET has been done. JokesBalance has been reset back to 10. ");
					});
				}
			}
			if (event.postback) {
				let text = JSON.stringify(event.postback)
				sendTextMessage(senderId, "Postback received: "+text.substring(0, 200), token)
				continue
			}
		}
	}

	res.sendStatus(200);
};

// access token as environmental var in .env
const token = process.env.PAGE_ACCESS_TOKEN;

function sendTextMessage(sender, text) {
	let messageData = { text:text };

	request({
		url: 'https://graph.facebook.com/v2.6/me/messages',
		qs: {access_token: token},
		method: 'POST',
		json: {
			recipient: {id:sender},
			message: messageData,
		}
	}, function(error, response, body) {
		if (error) {
			console.log('Error sending messages: ', error)
		} else if (response.body.error) {
			console.log('Error: ', response.body.error)
		}
	})
};
