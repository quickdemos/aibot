const fs = require('fs');

exports.root = function (req, res) {
	res.writeHead(200, {'Content-Type': 'text/html'});
	fs.createReadStream('public/index.html').pipe(res);
}

exports.termsofservice = function (req, res) {
		res.writeHead(200, {'Content-Type': 'text/html'});
		fs.createReadStream('public/termsofservice.html').pipe(res);
}

exports.privacypolicy = function (req, res) {
	res.writeHead(200, {'Content-Type': 'text/html'});
	fs.createReadStream('public/privacypolicy.html').pipe(res);
}
