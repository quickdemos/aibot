const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');

const routes = require('./routes');

const app = express();

// Enable CORS with various options
// https://github.com/expressjs/cors
app.use(cors());

// Parse incoming request bodies as application/x-www-form-urlencoded
// https://github.com/expressjs/body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


// Lets you use HTTP verbs such as PUT or DELETE
// https://github.com/expressjs/method-override
app.use(methodOverride());

// Mount API routes
app.use('', routes);

// run the  server
app.listen(process.env.PORT || 5000, function() {
	// eslint-disable-next-line no-console
	console.log(`
    Bot app running on Port: ${process.env.PORT}
    Env: ${process.env.ENV || 'dev'}
  `);
});

module.exports = app;
