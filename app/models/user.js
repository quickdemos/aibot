// const mongoose = require('mongoose');
import mongoose  from 'mongoose';
require('mongoose-long')(mongoose);

import Promise from 'bluebird';

// mongoose.Promise = Promise;
mongoose.Promise = Promise;

const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;

const UserSchema = new Schema({
	userId: {
		type: SchemaTypes.Long,
		required: true,
	},
	jokesBalance: {
		type: SchemaTypes.Number,
		required: false,
	},
	lastJokeSentAt: {
		type: SchemaTypes.Number,
		required: false,
	}
}, {
	timestamps: true,
});

const User = mongoose.model('User', UserSchema);

export default User;


