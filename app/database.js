const mongoose = require('mongoose');

// Connect to  mongo database;
mongoose.connect(process.env.MONGO_URI);
mongoose.connection.on('error', (err) => {
	throw err;
});
